<?php
include 'vendor/autoload.php';

/*
$app = new \Slim\Slim();
$app->get('/hello/:name', function ($name) {
    echo "Hello, $name";
});
$app->run();
*/

class MyMiddleware extends Slim\Middleware
{
	public function call()
	{
		$this->next->call();

		$this->app->response->body = strtoupper($this->app->response->body);
	}
}

\Slim\Route::setDefaultConditions(['name' => '^m']);

$app = new \Slim\Slim();

$app->group('/hello', function() use ($app) {
	$app->get('/:name', function($route) {
		var_dump($route);
	}, function ($name) use ($app) {
		echo "Hello, $name";
		$app->pass();
	})->name('foo');
});

$app->get('/hello/:name', function($name)
{
	echo "Here $name";
});

# var_dump($app->urlFor('foo', ['name' => 'matthew']));

$app->add(new MyMiddleware);

$app->run();