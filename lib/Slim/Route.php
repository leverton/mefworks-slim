<?php namespace Slim;

class Route extends \mef\MVC\PathRoute
{
	static private $defaultConditions;

	private $methods = [];
	private $name;
	private $conditions = [];
	private $middleware = [];

	static public function setDefaultConditions(array $conditions)
	{
		self::$defaultConditions = $conditions;
	}

	static public function getDefaultConditions()
	{
		return self::$defaultConditions;
	}

	public function __construct($routePath, $callback, $middleware = [])
	{
		$this->conditions = self::$defaultConditions;
		$this->middleware = $middleware;

		return parent::__construct($routePath, ['callback' => $callback, 'controller' => null, 'action' => null]);
	}

	public function via()
	{
		$this->methods = array_flip(func_get_args());

		return $this;
	}

	public function matches(\mef\MVC\Request $req)
	{
		if (!isset($this->methods[$req->method]))
		{
			return null;
		}

		$match = parent::matches($req);

		if (!$match)
		{
			return null;
		}

		foreach ($this->conditions as $key => $regexp)
		{
			if (!preg_match('#' . $regexp . '#', $match->params[$key]))
			{
				return null;
			}
		}

		return $match;

	}

	public function name($name)
	{
		$this->name = (string) $name;

		return $this;
	}

	public function getName()
	{
		return $this->name;
	}

	public function conditions(array $conditions)
	{
		$this->conditions = $conditions;
	}

	public function getMiddleware()
	{
		return $this->middleware;
	}
}