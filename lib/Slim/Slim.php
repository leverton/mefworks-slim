<?php namespace Slim;

class Slim extends Middleware
{
	private $request;
	private $response;

	private $routes = [];
	private $segments = [];
	private $middleware = null;

	public function __construct()
	{
		$this->request = \mef\HTTP\Request::getCurrent();
		$this->response = new \mef\HTTP\Response();
		$this->middleware = $this;
	}

	public function __get($key)
	{
		switch ($key)
		{
			case 'request':
				return $this->request;

			case 'response':
				return $this->response;
		}

		throw new \Exception("Invalid property $key");
	}

	public function group($segment, $callback)
	{
		$this->segments[] = $segment;
		$callback();
		array_pop($this->segments);
	}

	public function get()
	{
		return call_user_func_array([$this, 'map'], func_get_args())->via('GET');
	}

	public function post()
	{
		return call_user_func_array([$this, 'map'], func_get_args())->via('POST');
	}

	public function delete()
	{
		return call_user_func_array([$this, 'map'], func_get_args())->via('DELETE');
	}

	public function options()
	{
		return call_user_func_array([$this, 'map'], func_get_args())->via('OPTIONS');
	}

	public function patch()
	{
		return call_user_func_array([$this, 'map'], func_get_args())->via('PATCH');
	}

	public function put()
	{
		return call_user_func_array([$this, 'map'], func_get_args())->via('PUT');
	}

	public function map()
	{
		$args = func_get_args();
		$argc = count($args);

		$route = new Route(implode('/', $this->segments) . $args[0], $args[$argc - 1], array_slice($args, 1, $argc - 2));
		$this->routes[] = $route;

		return $route;
	}

	public function urlFor($name, $params)
	{
		foreach ($this->routes as $route)
		{
			if ($route->getName() == $name)
			{
				$parts = $route->getParts();

				foreach ($parts as $key => $val)
				{
					if ($val[0] === ':')
					{
						$parts[$key] = $params[substr($val, 1)];
					}
				}

				return implode('/', $parts);

				break;
			}
		}

		return null;
	}

	public function halt($code, $message = '')
	{
		$this->response->setStatus($code, $message);
		$this->response->body = $message;

		throw new Exception\Stop();
	}

	public function pass()
	{
		throw new Exception\Pass();
	}

	public function redirect($url, $code = 302)
	{
		$this->response->headers['Location'] = $url;
		$this->response->setStatus($code, 'Redirect');

		throw new Exception\Stop();
	}

	public function stop()
	{
		$this->response->body = ob_get_contents();

		throw new Exception\Stop();
	}

	public function add(Middleware $middleware)
	{
		$middleware->setApp($this);
		$middleware->setNext($this->middleware);

		$this->middleware = $middleware;
	}

	public function run()
	{
		$this->middleware->call();

		foreach ($this->response->headers as $key => $val)
		{
			header("$key: $val");
		}

		echo $this->response->body;
	}

	public function call()
	{
		$router = new \mef\MVC\RouteRouter($this->routes);

		ob_start();

		foreach ($router->match($this->request) as $match)
		{
			try
			{
				$route = $match->getRoute();

				foreach ($route->getMiddleware() as $middleware)
				{
					$middleware($route);
				}

				call_user_func_array(
					$match->params['callback'],
					array_values(array_diff_key($match->params, array_flip(['controller', 'action', 'callback'])))
				);

				$this->response->body = ob_get_clean();

				break;
			}
			catch (Exception\Pass $e)
			{
				ob_clean();
			}
			catch (Exception\Stop $e)
			{
				ob_end_clean();

				break;
			}
		}
	}
}