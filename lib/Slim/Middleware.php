<?php namespace Slim;

abstract class Middleware
{
	protected $app;
	protected $next;

	abstract public function call();

	final public function setApp(Slim $app)
	{
		$this->app = $app;
	}

	final public function setNext(Middleware $middleware)
	{
		$this->next = $middleware;
	}
}